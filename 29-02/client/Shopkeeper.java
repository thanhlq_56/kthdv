/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author hoyo
 */
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import service.Calculator;

public class Shopkeeper{
	
	public static void main(String[] args) throws Exception {
	   
            URL url = new URL("http://localhost:9999/ws/calculator?wsdl");
	
        //1st argument service URI, refer to wsdl document above
	//2nd argument is service name, refer to wsdl document above
        QName qname = new QName("http://service/", "CalculatorImpService");

        Service service = Service.create(url, qname);

        Calculator hello = service.getPort(Calculator.class);

        System.out.println("Result of 123 + 456 = " + hello.calculate(123, 456, '+'));
        System.out.println("Edit code for more fun!");
    }

}
