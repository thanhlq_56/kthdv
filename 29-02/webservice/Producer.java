

import javax.xml.ws.Endpoint;
import service.CalculatorImp;

//Endpoint publisher
public class Producer{
	
	public static void main(String[] args) {
	   Endpoint.publish("http://localhost:9999/ws/calculator", new CalculatorImp());
	   System.out.println("Server running at: http://localhost:9999/ws/calculator");
    }
}