/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import javax.jws.WebService;
 
//Service Implementation
@WebService(endpointInterface = "service.Calculator")
public class CalculatorImp implements Calculator{
    
            @Override
	    public String calculate(float a, float b, char c){
                
                switch ( c ){
                    case '*':
                        return a * b + "";
                    case '+':
                        return a + b + "";
                    case '-':
                        return a - b + "";
                    case '/':
                        return a / b + "";
                }
                
                return "Invalid input!";
            };

        
        
 
}